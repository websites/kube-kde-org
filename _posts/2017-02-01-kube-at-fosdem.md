---
date: '2017-02-01 11:14:00'
layout: post
title: Kube at FOSDEM 2017
tags:
- Kube
- KDE
- PIM
- Kolab
---

If you want to hear more about Kube come to FOSDEM over the upcoming weekend.

You can find out more on the Kolab and KDE booth, and there will be a talk on Sunday 5. February, at 16:20 in Room K.4.401.

See you there!
