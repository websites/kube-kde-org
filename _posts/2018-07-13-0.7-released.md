---
date: '2018-07-13 08:14:00'
layout: post
title: Kube 0.7.0 released
tags:
- Kube
- KDE
- PIM
- Kolab
---

Kube 0.7.0 has been released.

Find out more here: [https://cmollekopf.wordpress.com/2018/07/12/kube-0-7-0-is-out/](https://cmollekopf.wordpress.com/2018/07/12/kube-0-7-0-is-out/)
