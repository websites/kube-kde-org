---
date: '2017-10-04 11:14:00'
layout: post
title: Kube for Kolab Now is now avialable
tags:
- Kube
- Kolab
---

Kube for Kolab Now has been released.

Find out more here: [https://blogs.kolabnow.com/2017/09/27/kube-for-kolab-now](https://blogs.kolabnow.com/2017/09/27/kube-for-kolab-now)

And for some background information: [https://cmollekopf.wordpress.com/2017/10/05/kube-finding-our-focus/](https://cmollekopf.wordpress.com/2017/10/05/kube-finding-our-focus/)
