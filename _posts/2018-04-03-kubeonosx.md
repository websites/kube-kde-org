---
date: '2018-04-04 12:00:00'
layout: post
title: Kube now available on Mac OS
tags:
- Kube
- KDE
- PIM
- Kolab
- Mac
---

Kube is now available on Mac OS.

Find out more here: [https://blogs.kolabnow.com/2018/04/03/kube-on-mac-os](https://blogs.kolabnow.com/2018/04/03/kube-on-mac-os)
